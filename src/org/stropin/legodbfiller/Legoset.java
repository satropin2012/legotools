package org.stropin.legodbfiller;

import java.util.HashMap;
import java.util.Map;

public class Legoset {
	public int id;
	public String group;
	public Map<String,Integer> details = new HashMap<String, Integer>();

	public Legoset(int id, String group) {
		this.id = id;
		this.group = group;
	}

}
