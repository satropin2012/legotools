package org.stropin.legodbfiller;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.View;

public class dbmain extends Activity {
	
	public static final int DIALOG_DOWNLOAD_PROGRESS = 0;	  
	    private ProgressDialog mProgressDialog;
	    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	/** � ���� ����������� �������������  ����� ��� ����� �.�. ���� � ������������� ���������� ������� ���������
	 * ����� �� ������ ��� ��� �������� � ������� ����������� ���
	 * ����� � ������� ���� ���� ������ ������ ����� ������(��� ������������� �������� �� ������������� ����� � �������) ��� ������� ��������� ���������
	 * ��� ������� ������ �����! */
	public void normdbfile(View view) {
		
//������ ���� ���� � ��������� �� � ����
		boolean ok = true;
		String line = "";
	    BufferedReader br = null;
	    
		 try {
		        FileInputStream fis = new FileInputStream(Environment
		                .getExternalStorageDirectory().getAbsolutePath()
		                + "/allbase.properties");
		        br = new BufferedReader(new InputStreamReader(fis));
		    } catch (FileNotFoundException e) {
		        ok = false;
		        e.printStackTrace();
		    }     

	 
//�������� ���������������
		 //�������  ���� �� �����  � � ����� ���� ��  �����+�����
		 //����� ������� ���������� ��� ������������� ����� � ������ ����		 		 		 		 
		 
		 
		 Map<Integer,Legoset> allset = new HashMap<Integer,Legoset>();		 
		 
		 try {
			while ((line = br.readLine()) != null) {
			        String[] body = line.split("="); //id �������� �� ��� ������ �����������
			        String[] col = body[1].split(" ");					 
//����� ����� ���-�� ��������
			       String did = col[0];
			       int setid = Integer.valueOf(col[1]);
			       int cnt = Integer.valueOf(col[2]);
			       String grid = col[3]; 
			      
			       Legoset lset = allset.get(setid);
//��� ��� ���� ����� �������� ������ ��������			       
			       if(lset!=null) {
//���� �������� ���� �� ����� ������ � ���� ���� ��������� ���������� � ���� ��� ��������			    	   
			    	   Integer havecnt = lset.details.get(did);
			    	   if(havecnt==null) {
			    		   lset.details.put(did, cnt);
			    		   Log.v("norm", "�������� � ���� " + String.valueOf(setid) + " ������ " + did +" ���-�� " + String.valueOf(cnt));
			    	   } else {
			    		   lset.details.put(did, cnt + havecnt);
			    		   Log.v("norm", "��������� � ���� " + String.valueOf(setid) + " ������ " + did +" ���-�� ����:"+String.valueOf(havecnt) + " ���������� " + String.valueOf(cnt)); 
			    	   }			    	   
			       } else {
//���� ��� ��� ������ � ���������� ������
			    	   lset = new Legoset(setid, grid);
			    	   lset.details.put(did, cnt);
			    	   Log.v("norm", "������� ��� " + String.valueOf(setid) + " ������ " + did +" ���-�� "+ String.valueOf(cnt));
			    	   allset.put(setid, lset);
			       }			       			        			        
			 }
		} catch (IOException e) {		
			e.printStackTrace();
		}

		 
//������� ���������� �� � ����...
		 int index =1; //��������� 
		 int setcount = allset.size();
		   FileOutputStream fos = null;
		try {
			fos = new FileOutputStream( Environment.getExternalStorageDirectory().toString() + "/normal.pref" );
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} 
           OutputStreamWriter osw = new OutputStreamWriter(fos); 

           
		//�� ���� ������ ������ ��� �������� ����
		 Iterator<Entry<Integer, Legoset>> it = allset.entrySet().iterator();
	     Entry<Integer, Legoset> e;
	     Legoset l;
	     
	     int i=0;
	        while(it.hasNext()) {
	        	i++;
	        	Log.v("norm", String.valueOf( (float)i/setcount*100 ) + "% ������ �����");
	            e = it.next();
	            int setid = e.getKey();
	            l =  e.getValue();
	          //index=����� ����� ���-�� ��������
	            
//������������ �� ������ ���� ��� ������ ���������� ���������� ���
	            	
	            Iterator<Entry<String, Integer>> itdet = l.details.entrySet().iterator();
	            Entry<String, Integer> det;		       
	            while(itdet.hasNext()) {
		            det = itdet.next();
		            String did = det.getKey();
		            int cnt = det.getValue();
		            
		            String finalrow = String.valueOf(index++) + "=" + did + " " + String.valueOf(l.id) + " " + String.valueOf(cnt) + " " + l.group;
		            		            		
		            try {
						osw.write(finalrow + "\n");
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} 
		        }	            	            	           	             
	            
	        }
                    
        try {
			osw.flush();
			osw.close(); 
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} 
          
		 
		 
	}
	
/** ��������� ������ �� ����� ������� ��� ���� � ���� */	
	public Set<String> createsetlist(View view) {		
		//������ ���� ���� � ��������� �� � ����
				boolean ok = true;
				String line = "";
			    BufferedReader br = null;
			    
				 try {
				        FileInputStream fis = new FileInputStream(Environment
				                .getExternalStorageDirectory().getAbsolutePath()
				                + "/allbase.properties");
				        br = new BufferedReader(new InputStreamReader(fis));
				    } catch (FileNotFoundException e) {
				        ok = false;
				        e.printStackTrace();
				    }     
			 				 
				 Set<String> allset = new HashSet<String>();		 
				 
				 try {
					while ((line = br.readLine()) != null) {
					        String[] body = line.split("="); //id �������� �� ��� ������ �����������
					        String[] col = body[1].split(" ");					 
					        String setid = col[1];
					      
					       allset.add(setid);			       			        			        
					 }
				} catch (IOException e) {		
					e.printStackTrace();
				}
				
				return allset; 
				 
				 /* ���������� � ���� ����� ������ ��� �� �����
				String all = null;
				for(String str:allset)
					all += str + " ";
			
				
				FileOutputStream fos;
				try {
					fos = new FileOutputStream( Environment.getExternalStorageDirectory().toString() + "/allsets.txt" );
					OutputStreamWriter osw = new OutputStreamWriter(fos); 
					osw.write(all);					
					osw.flush(); 
					osw.close(); 
				} catch (IOException e) {
					e.printStackTrace();
				} 
              	*/				
	}
	
	
	/** ������� ���� ��� ��������� ����*/
	public void createsetnames(View view) {
		String newsets = 
				   
   			 //sw
   			 "75148 75149 75098 30602 911610 5004406 911609 30605 30277 911608 911607"
   		//town
   			 + " 30313 60132 60134 30346 30347 30348 30349 60131 60109 60112 60108 60110 60129 60128"
   	//Igor Tsapir	 60128
   	//email 30313 + jurassic cat 
   			 
   			 //creator
   			 + " 40221 31049 30471 4250"
   			 //mixels
   			 + " 41564 41565 41568 41566 41563 41569 41571"
   			 //friends
   			 + " 41123"
   			 //minecraft
   			 + " 21128 21124 21123 21125 21126"
   			 //ideas
   			 + " 21305"
   			 //speed champ
   			 + " 75876 75871 75875 75870 75874 75873 75872"
   			 //super hero
   			 + " 76034 76034"
   			 //friends
   			 + " 41135"//������� ������� �������� �����, 4 ��� 2016 �. � 5:30 9 0  �������� 41135
   	 		
   			 //Jurassic World new cat
   			 + " 30320 75916 75919 75915 75920 75917 75918"
   			 //email from Wes Stanki 10220 new cat sculptures
   			 + " 10220 10214 10226 10187 10231"
   			 
   			 //bionicle
   			 + " 71309 71304"; //email 20marta
		
		createnameyearlistfile(newsets);
	}
	
private void createnameyearlistfile(String newsets) {
		
		String line = "";
	    BufferedReader br = null;
	    
		 try {
		        FileInputStream fis = new FileInputStream(Environment
		                .getExternalStorageDirectory().getAbsolutePath()
		                + "/sets.csv");
		        br = new BufferedReader(new InputStreamReader(fis));
		    } catch (FileNotFoundException e) {
		        e.printStackTrace();
		    }     

//�� � ���� ��������� �� �����
//map<"setid","setyear;setname">		 
		 Map<String,String> setlist = new HashMap<String,String>();		 
//����� 3 ���������� ��� ��� � �������� ��� ����� ����� � ���� ������ ������������
		 ArrayList<String> arraysetid = new ArrayList<String>();
		 ArrayList<String> arraysetyear = new ArrayList<String>();
		 ArrayList<String> arraysetname = new ArrayList<String>();
		 
		 try {
			while ((line = br.readLine()) != null) {
			        String[] body = line.split(","); 
			        String setid = body[0];					 
			        String setyear = body[1];
			        String setname = body[6];
			        setlist.put(setid, setyear +";"+ setname);
			 }
		} catch (IOException e) {		
			e.printStackTrace();
		}
		
//������ �������� ������ ����� � ����
//		 String q="SELECT DISTINCT "+ SQLhelper.TABLE_INSTRUCTIONS_SETID +" FROM " + SQLhelper.TABLE_INSTRUCTIONS; 
//		 Cursor cursets = db.rawQuery(q, null);	   		
//		 cursets.moveToFirst();		
//		 do {		
//			 String sid = cursets.getString(0);
			
//�� ��� ��������		 

		 String[] sl = newsets.split(" ");
		 for(String sid:sl) {
		 
			 if(setlist.containsKey( sid + "-1" )) {
				 
				 arraysetid.add(sid);
				 
				 String val = setlist.get(sid+ "-1");
				 String[] vlist = val.split(";");
				 
				 arraysetyear.add(vlist[0]);
				 arraysetname.add(vlist[1]);				 
				 
			 } else {
				 Log.v("loadsetname","��� ���� " + sid);
				 
			 }
			 				    			    							
		 }  //	 while(cursets.moveToNext());		 
//		 cursets.close();
	
//��������� � ����
		 
		 try{
		   FileOutputStream fos = new FileOutputStream( Environment.getExternalStorageDirectory().toString() + "/setsyearname.xml" ); 
           OutputStreamWriter osw = new OutputStreamWriter(fos); 
              		                       
           osw.write("<string-array name=\"setid\">" + "\n");
           for(String arsetid:arraysetid) {
           		osw.write( "<item>" + arsetid + "</item>" + "\n"); 
           }
           osw.write("</string-array>" + "\n");

           osw.write("<string-array name=\"setyear\">" + "\n");
           for(String str:arraysetyear) {
           		osw.write( "<item>" + str + "</item>" + "\n"); 
           }
           osw.write("</string-array>" + "\n");

           osw.write("<string-array name=\"setname\">" + "\n");
           for(String str:arraysetname) {
           		osw.write( "<item>" + str + "</item>" + "\n"); 
           }
           osw.write("</string-array>" + "\n");
		 
           osw.flush(); 
           osw.close();
		 } catch (Exception e) {
			 e.printStackTrace();
		} 
	}

	
	public void startDownload(View view) {
	        String url = "h";	                   
            mProgressDialog = new ProgressDialog(this);
			//mProgressDialog.setMessage(" file..");
			mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			mProgressDialog.setCancelable(false);
			mProgressDialog.show();	 						
	        new DownloadFileFromURL().execute(url);
	 }
	 
	    
	    /**
	     * Background Async Task to download file
	     * */
	    class DownloadFileFromURL extends AsyncTask<String, String, String> {

	        /**
	         * Before starting background thread Show Progress Bar Dialog
	         * */
	        @Override
	        protected void onPreExecute() {
	            super.onPreExecute();
           
	        }

	        protected void loadimg(String iurl, String name) throws IOException {
	        	
	        //	String filepath = Environment.getExternalStorageDirectory().toString()
            //           ;	
	        		        	
	        	URL url = new URL(iurl);
                URLConnection conection = url.openConnection();
                conection.connect();

                // this will be useful so that you can show a tipical 0-100%
                // progress bar
                int lenghtOfFile = conection.getContentLength();

                // download the file
                InputStream input = new BufferedInputStream(url.openStream(),
                        8192);
                
                // Output stream

                // create a File object for the parent directory
                File fimg = new File(Environment.getExternalStorageDirectory().toString() + "/img/");
                // have the object build the directory structure, if needed.
                fimg.mkdirs();
                // create a File object for the output file
                File outputFile = new File(fimg, name +".jpg");
                // now attach the OutputStream to the file object, instead of a String representation
                OutputStream output = new FileOutputStream(outputFile);                                

                byte data[] = new byte[1024];

                long total = 0;
                int count=0;
                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                //    publishProgress("" + (int) ((total * 100) / lenghtOfFile));
                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();	               	               
                // closing streams
                output.close();
                input.close();
	        }
	        
	        void DeleteRecursive(File fileOrDirectory) {
	            if (fileOrDirectory.isDirectory())
	                for (File child : fileOrDirectory.listFiles())
	                    DeleteRecursive(child);

	            fileOrDirectory.delete();
	        }
	        
	        /**
	         * Downloading file in background thread
	         * */
	        @Override
	        protected String doInBackground(String... f_url) {
	            int count, j=0;
	            String[] arr = null;
	         	long ID = 0;//�� ����� ����������� �� � ��� �� ��� ����� ������ ��������� ��� ����� � 0 ����������
	             
	         	String newsets =
           			 "";     
           	
	         	
	            try {
	            	 Set<String> sethave = createsetlist(null);   	 
	            	 
	            	  
	           /* 	18/6/16 
        			 //sw
        			 "75148 75149 75098 30602 911610 5004406 911609 30605 30277 911608 911607"
        		//town
        			 + " 30313 60132 60134 30346 30347 30348 30349 60131 60109 60112 60108 60110 60129 60128"
        	//Igor Tsapir	 60128
        	//email 30313 + jurassic cat 
        			 
        			 //creator
        			 + " 40221 31049 30471 4250"
        			 //mixels
        			 + " 41564 41565 41568 41566 41563 41569 41571"
        			 //friends
        			 + " 41123"
        			 //minecraft
        			 + " 21128 21124 21123 21125 21126"
        			 //ideas
        			 + " 21305"
        			 //speed champ
        			 + " 75876 75871 75875 75870 75874 75873 75872"
        			 //super hero
        			 + " 76034 76034"
        			 //friends
        			 + " 41135"//������� ������� �������� �����, 4 ��� 2016 �. � 5:30 9 0  �������� 41135
        	 		
        			 //Jurassic World new cat
        			 + " 30320 75916 75919 75915 75920 75917 75918"
        			 //email from Wes Stanki 10220 new cat sculptures
        			 + " 10220 10214 10226 10187 10231"
        			 
        			 //bionicle
        			 + " 71309 71304" //email 20marta
    
        		*   parts nety  911610 5004406 
	           
	            	75915 ��������!
	
        		*/ 	 
	            	 
	            	 
	          //  ��������, ����������, ����� Nexo Knights 70332
	              	 
	            	 
	            	 //�������� ���� �� ��������� ������ ����� <gorox04011973@gmail.com> 76039, ���� ���������76034 � 44024.
	        
	            	 
	            	 //������� ���������� � ������� �������
	            	 //�������� ���������� 42009 ultimate
	            	 
	            	 //��������� 75908  
	            	//70794 70165 ��� ���� ���������	            			 
	            			 	            	
	            	 //3844 ���� �� ���������
	            	

        			 //Star Wars: Star Wars Episode 7: 5002948-1 "5002948 5002947 5004735 60096 5004809 5002929 66526"
        			 
        			 //��� ���� 10593
	            	 //http://rebrickable.com/sets/9391-1-b1/bulldozer-technic-2012
	            	 
	            	//hero factory cat http://www.bricklink.com/catalogList.asp?pg=1&catString=749&catType=S&v=1
	            	 //��� ������� 8884 8885 8886 8887;
	            	 //������� 5000281  708010  708011  708012 708013 708014 708015 708016   5002045 5002203 5002204       			 
	            	//		 ��� ��������//""; 
	            			 //"";
	            	 //";
	            	 
	            	 //  850617 850618";
//" 40028 4736 4737 4738 4840 4842 5378 4762 4766 4767 4768 4768 4695 4750 4751 4752 4753 4754";
	            	//town err 60031 
	            	//town none 30313 60076   60062
	            	//75899 75908 75909 75910 75913         		            	
	            	//not inv 70410 
	            	//not have 79092 14001 9601
	            	String filepath = Environment.getExternalStorageDirectory().toString()
	                         + "/setinfo.html";	  
	            	 
	            	 ArrayList<String> itemlist = new ArrayList<String>();	            	 	            	
	            	 
//�������� ��������� ����� ��� �������� �������� �����	            	 	                 
	            	 File fimg = new File(Environment.getExternalStorageDirectory().toString() + "/img/");
		              // have the object build the directory structure, if needed.
	            	 DeleteRecursive(fimg);	            	 
		             fimg=null;
	            	 	            	
	            	 arr = newsets.split(" ");
	            	 mProgressDialog.setMax(arr.length);
	            	 for(j=0; j<arr.length;j++) {
	            		 if(sethave.contains(arr[j])) {
	            			 Log.v("add", "����������, ��� ���� " + arr[j]);
	            			 continue;
	            		 }
	            		 
	            		 publishProgress(String.valueOf(j));
	            		 String setid = arr[j];
	            		 Log.v("add", "add " + arr[j]);
	            		 
	            		 //3858
	            		 			//	http://www.bricklink.com/catalogItemInv.asp?G="+setid+"&v=1&bt=0&sortBy=3&sortAsc=D&viewID=Y
	            		
	            	ArrayList<String> urls = new ArrayList<String>();
	            	urls.add("http://www.bricklink.com/catalogItemInv.asp?S="+setid+"-1&v=0&bt=0&viewID=Y&sortBy=3&sortAsc=D");
	            	urls.add("http://www.bricklink.com/catalogItemInv.asp?G="+setid+"&v=0&bt=0&viewID=Y&sortBy=3&sortAsc=D");
	            	int catpos=-1;
	            	String setinfo = null;
	            	int url_count=0;
	            	for(String str_url:urls) {	            			            			            		
//��������� ��������� ����	        
	            		url_count++;
		            	URL url = new URL(str_url);
		                URLConnection conection = url.openConnection();
		                conection.connect();

		                int lenghtOfFile = conection.getContentLength();
		                InputStream input = new BufferedInputStream(url.openStream(), 8192);
		                OutputStream output = new FileOutputStream(filepath);
		                byte data[] = new byte[1024];
		                long total = 0;
		                while ((count = input.read(data)) != -1) {
		                    total += count;
		                    // publishing the progress....
		                    // After this onProgressUpdate will be called	                  
		                  //  publishProgress("" + (int) ((total * 100) / lenghtOfFile));
		                    // writing data to file
		                    output.write(data, 0, count);
		                }

		                // flushing output
		                output.flush();	               	               
		                // closing streams
		                output.close();
		                input.close();		                	                	                
//������	                
		                setinfo = getStringFromFile(filepath);	            		            		
	            		
//���� ����� ������ ���� ������ ������� ���� - ����� ��� ��������   ��������� ����  (�������� ������ � ��)
//���� �� ����� ���� �� ����. ���� �������� �� ������ ����� ���
		                catpos = setinfo.indexOf("catString=");		                		                
		                if(catpos!=-1) break;	            			    		 		                	 	            
	            	}
	            	
//���� -1 ������ �� ���� ����� ������� ���	            	
                	if(catpos==-1) {
                		Log.v("add", "������� �� ���, " + arr[j]);
                		continue;
                	}
                	                			            		  	 	            	 	            		                
//�������������� ��� ��� <item>2780 42030 285 36</item>
	                               	                	                
	                String catraw = setinfo.substring( catpos, catpos +15);	                	                	                	                
	                
	                int cat = Integer.parseInt(catraw.replaceAll("[\\D]",""));
	                
	                int posstart = setinfo.indexOf("<B>Parts:</B>");
	                int posend = setinfo.indexOf("</TABLE><P>");
	                
	              //  Log.v("add", arr[j] +  " posstart " + posstart + " posend " + posend );
	                
	                if(posstart==-1) {
	                	Log.v("add", "������� <B>Parts:</B>, " + arr[j]);
                		continue;
	                }
	                String cut = setinfo.substring(posstart, posend);
	                
	                cut = cut.replaceAll("&nbsp;", ""); 
	                cut = cut.replaceAll("&#40;", "(");
	                cut = cut.replaceAll("&#41;", ")");
	                
	                cut = cut.replaceAll("<TR BGCOLOR=\"#FFFFFF\"><TD ALIGN=\"RIGHT\">", "\n<item>");
	                cut = cut.replaceAll("<TR BGCOLOR=\"#EEEEEE\"><TD ALIGN=\"RIGHT\">", "\n<item>");
	                			
	                cut = cut.replaceAll("</TD><TD ALIGN=\"CENTER\"><B><FONT COLOR='Green'>Yes</FONT></B></TD><TD ALIGN=\"RIGHT\">", "�");
	                cut = cut.replaceAll("</TD><TD NOWRAP><A HREF=\"http://alpha.bricklink.com/pages/clone/catalogitem.page\\?P=","�");//"</TD><TD NOWRAP><A HREF=\"catalogItem.asp\\?P=", "�");
	                cut = cut.replaceAll("</TD><TD NOWRAP><A HREF=\"/v2/catalog/catalogitem.page\\?P=","�");
	                cut = cut.replaceAll("</TD><TD NOWRAP><A HREF=\"/v2/catalog/catalogitem.page\\?M=","�");
	                cut = cut.replaceAll("</TD><TD NOWRAP><A HREF=\"/v2/catalog/catalogitem.page\\?G=","�");
	                cut = cut.replaceAll("</TD><TD NOWRAP><A HREF=\"/v2/catalog/catalogitem.page\\?B=","�");
	       	         
	                //�� ��� gear
	                cut = cut.replaceAll("</TD><TD NOWRAP><A HREF=\"http://alpha.bricklink.com/pages/clone/catalogitem.page\\?G=", "�");
	                
	                //������
	                cut = cut.replaceAll("</TD><TD NOWRAP><A HREF=\"http://alpha.bricklink.com/pages/clone/catalogitem.page\\?B=","�");
	                //�����������
	                
	                cut = cut.replaceAll("</TD><TD NOWRAP><A HREF=\"http://alpha.bricklink.com/pages/clone/catalogitem.page\\?M=","�");//"</TD><TD NOWRAP><A HREF=\"catalogItem.asp\\?M=","�");
	                cut = cut.replaceAll( Pattern.quote( "</A> (<A HREF=\"catalogItemInv.asp\\?M="),"");
	                
	                cut = cut.replaceAll("&idColor=", "�");
	                
	                cut = cut.replaceAll( Pattern.quote( "\">Inv</A>)</TD><TD>"),"");
	                
	                cut = cut.replaceAll("</A></TD><TD>", "�");
	               
	                cut = cut.replaceAll("<img src='/Images/new_icon.png' width='30' border='0' alt='Added on ", "");
	                cut = cut.replaceAll("' title='Added on ", "�");
	                cut = cut.replaceAll("'>", "�");
	                //10/16/2014' title='Added on 10/16/2014'>
	                
	                cut = cut.replaceAll( Pattern.quote("<TR BGCOLOR=\"#C0C0C0\"><TD COLSPAN=\"6\" ALIGN=\"CENTER\"><FONT FACE=\"Tahoma,Arial\" SIZE=\"2\"><B>Books:</B></FONT></TD></TR>"), "");
	                cut = cut.replaceAll( Pattern.quote("<TR BGCOLOR=\"#C0C0C0\"><TD COLSPAN=\"6\" ALIGN=\"CENTER\"><FONT FACE=\"Tahoma,Arial\" SIZE=\"2\"><B>Minifigs:</B></FONT></TD></TR>"), "");
	                
	                cut = cut.replaceAll("</TD><TD ALIGN=\"RIGHT\"></TD></TR>", "</item>");
	                					//������ ����� </td> �����!
	                int start = cut.indexOf("<item>");
	                int end = cut.indexOf("#000000");
	                
	             //   Log.v("add", arr[j] +  " <item>start " + start + " #000000end " + end );
		             
	                
	                if(end==-1) cut = cut.substring(start); 
	                else {
	                	cut = cut.substring(start, end);	                	                
	                	cut = cut.substring(0, cut.lastIndexOf("</item>") +7 );
	                }
	                String[] sl = cut.split("\n"); 
	                
	                int itemcount =  sl.length;


	                
	                for(int i=0; i<itemcount; i++) {  //<item>603585�12�4073">4073�Trans-Red Plate, Round 1 x 1 Straight Side</item>
	                	//if(i==110)
	                	//	Log.e("add", i + "  : " + sl);
	                	
	                	String[] row = sl[i].split("�");
	                	//row[0] 603585  ���� �������
	                	//row[1] 12		 �����	
	                	//row[2]  ������ ��
	                	//row[3] &idColor=11">4073
	                	//row[2] 4073">4073   ( split("\">") )    ��������
	                	//row[3]  
	                	//row[4]	                	
	                	try {
	                	String finalrow = String.valueOf(++ID) + "=";	                	
	                	finalrow += row[2].split("\">")[0] + " ";//row[2]+" ";
	                	finalrow += setid + " ";
	                	finalrow += row[1] + " ";
	                	finalrow += String.valueOf(cat);// + "</item>";
	                	
	                	itemlist.add(finalrow);
	                	} catch (Exception e) {
	    	                Log.e("add","err " + arr[j] + " " + e.getMessage()+ "sl[i]="+ sl[i]);
	    	            }
	                	
	                	//���� �������   <item>2780 42030 285 36</item>	
	                	
	                }
	                
	              //�������� ����	                	           	               	                
	    /*            try {
	                	loadimg( "http://www.bricklink.com/S/"+setid+"-1.jpg","s"+setid);
	                } catch(Exception ex) {
	                	try{
	                	//Log.e("Error: ", "http://www.bricklink.com/S/"+setid+"-1.jpg");
	                	loadimg( "http://www.bricklink.com/S/"+setid+"-1.gif","s"+setid);	                	
	                	} catch(Exception ex1) {
		                	Log.e("Error: ", "http://www.bricklink.com/S/"+setid+"-1.gif");
		                }
	                }
	     */          
	                try {
	                	if(url_count==2) //�� ���2 ������ ���� �� ��������
	                		loadimg( "http://www.bricklink.com/GL/"+setid+".jpg?1","h"+setid);
	                	else
	                		loadimg( "http://www.bricklink.com/SL/"+setid+"-1.jpg","h"+setid);
	                	
	                } catch(Exception ex) {
	                	Log.e("ERROR", "�������� � ���� "+setid+" �� ������������");
	                }
	               
	           //     http://www.bricklink.com/S/42002-1.jpg  
	           //     http://www.bricklink.com/SL/42002-1.jpg
	                	                
	            	} //����. ��� 

	                
	                FileOutputStream fos = new FileOutputStream( Environment.getExternalStorageDirectory().toString() + "/newsets.xml" ); 
	                OutputStreamWriter osw = new OutputStreamWriter(fos); 
	                for(int i=0; i<itemlist.size(); i++) {
	                	osw.write(itemlist.get(i) + "\n"); 
	                }
	                osw.flush(); 
	                osw.close(); 	                
	                
	            } catch (Exception e) {
	                Log.e("add","err " + arr[j] + " " + e.getMessage());
	            }

	            
	            createnameyearlistfile(newsets);//� ������ � ������� ��� ��������! ��������� ������!
	    		
	            
	            
	            return null;
	        }

	        
	        
	        public String getStringFromFile (String filePath) throws Exception {
	            File fl = new File(filePath);
	            FileInputStream fin = new FileInputStream(fl);
	            String ret = convertStreamToString(fin);
	            //Make sure you close all streams.
	            fin.close();        
	            return ret;
	        }
	        
	        public String convertStreamToString(InputStream is) throws Exception {
	            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
	            StringBuilder sb = new StringBuilder();
	            String line = null;
	            while ((line = reader.readLine()) != null) {
	              sb.append(line).append("\n");
	            }
	            reader.close();
	            return sb.toString();
	        }
	        
	        /**
	         * Updating progress bar
	         * */
	        protected void onProgressUpdate(String... progress) {
	            // setting progress percentage
	        	mProgressDialog.setProgress(Integer.parseInt(progress[0]));
	        }

	        /**
	         * After completing background task Dismiss the progress dialog
	         * **/
	        @Override
	        protected void onPostExecute(String file_url) {
	            // dismiss the dialog after the file was downloaded	            
	            mProgressDialog.dismiss();
	        }

	    }
	    
}
